Source: flare-engine
Section: games
Priority: optional
Maintainer: Manuel A. Fernandez Montecelo <mafm@debian.org>
Uploaders: Martin Quinson <mquinson@debian.org>
Build-Depends: debhelper-compat (= 13),
               cmake,
               libsdl2-dev,
               libsdl2-image-dev,
               libsdl2-mixer-dev,
               libsdl2-ttf-dev
Standards-Version: 4.6.2
Homepage: https://flarerpg.org/
Vcs-Git: https://salsa.debian.org/games-team/flare-engine.git
Vcs-Browser: https://salsa.debian.org/games-team/flare-engine

Package: flare-engine
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         fonts-liberation,
         fonts-unifont
Recommends: flare-game (>= ${source:Version})
Description: game engine for single-player 2D action role-playing games
 Flare (Free Libre Action Roleplaying Engine) is a simple game engine built to
 handle a very specific kind of game: single-player 2D action RPGs. Flare is not
 a reimplementation of an existing game or engine. It is a tribute to and
 exploration of the action RPG genre.
 .
 Flare uses simple file formats (INI style config files) for most of the game
 data, allowing anyone to easily modify game contents. Open formats are
 preferred (png, ogg). The game code is C++.
 .
 This package contains the game engine and you will need a game
 package (such as flare or polymorph) to play.
